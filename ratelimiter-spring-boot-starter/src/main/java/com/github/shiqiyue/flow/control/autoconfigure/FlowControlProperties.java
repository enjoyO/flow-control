package com.github.shiqiyue.flow.control.autoconfigure;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

import com.github.shiqiyue.flow.contol.entity.FlowControlItem;

@ConfigurationProperties(prefix = "flow.control")
public class FlowControlProperties {
	
	/** 是否启用流量控制功能 */
	private boolean enabled = true;
	/** redis地址 */
	private String redisUrl = "redis://localhost:6379";
	/** 拦截后返回给客户端的json字符串 */
	private String jsonResponse = "{\"code\":400,\"mes\":\"\u8BBF\u95EE\u592A\u8FC7\u9891\u7E41\uFF0C\u8BF7\u8FC7\u6BB5\u65F6\u95F4\u518D\u8BD5\"}";
	/** 过滤器拦截的url */
	private String[] filterUrlPatterns = { "/*" };
	
	private List<FlowControlItem> items;
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public String getRedisUrl() {
		return redisUrl;
	}
	
	public void setRedisUrl(String redisUrl) {
		this.redisUrl = redisUrl;
	}
	
	public String getJsonResponse() {
		return jsonResponse;
	}
	
	public void setJsonResponse(String jsonResponse) {
		this.jsonResponse = jsonResponse;
	}
	
	public String[] getFilterUrlPatterns() {
		return filterUrlPatterns;
	}
	
	public void setFilterUrlPatterns(String[] filterUrlPatterns) {
		this.filterUrlPatterns = filterUrlPatterns;
	}

	public List<FlowControlItem> getItems() {
		return items;
	}

	public void setItems(List<FlowControlItem> items) {
		this.items = items;
	}
	
}
