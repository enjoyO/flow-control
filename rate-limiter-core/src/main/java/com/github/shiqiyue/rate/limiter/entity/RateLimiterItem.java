package com.github.shiqiyue.rate.limiter.entity;

import java.util.UUID;

/***
 * 流量控制项
 * 
 * @author wwy
 *
 */
public class RateLimiterItem {
	
	private String id;
	
	/** ant表达式 */
	private String expl;
	
	/** 访问次数上限 */
	private Integer maxCallTimes;
	
	/** 时间范围 */
	private Long expires;
	
	public RateLimiterItem() {
	}
	
	public RateLimiterItem(String expl, Integer maxCallTimes, Long expires) {
		this.expl = expl;
		this.maxCallTimes = maxCallTimes;
		this.expires = expires;
		this.id = UUID.randomUUID().toString();
	}
	
	public String getExpl() {
		return expl;
	}
	
	public void setExpl(String expl) {
		this.expl = expl;
	}
	
	public Integer getMaxCallTimes() {
		return maxCallTimes;
	}
	
	public void setMaxCallTimes(Integer maxCallTimes) {
		this.maxCallTimes = maxCallTimes;
	}
	
	public Long getExpires() {
		return expires;
	}
	
	public void setExpires(Long expires) {
		this.expires = expires;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
}
